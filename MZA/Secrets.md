#Secrets

The global pipeline is deploying the secrets.
Secrets MSH and MZA
We are configuring the secrets manually.

key per team or key person
Add a step in the pipeline that checks for clear text passwords.
Sops Operator


about the terraform secrets ask stefan he said we can remove them
1. The devs should encrypt




Sops

Sops does encrypt by it self. It relies on other encryption technology. (AWS,KMS,GPG,AGE)

1. Creata the with age
`age-keygen -o key.txt`

2. Move the key in to .sops folder in users home directory.
`mv key.txt ~/.sops`

3. We can also add an environmental variable so that we don't have to specify the key each time.
`export SOPS_AGE_KEY_FILE=$HOME/.sops/key.txt`

4. Encrypt the secret.

sops --encrypt --age $(cat $SOPS_AGE_KEY_FILE | grep -oP "public key: \K(.*)") --encrypted-regex '^(data|stringData)$' --in-place ./secret.yaml

The stringData is the string we want to encrypt.

The encrypted file looks like this: 

`apiVersion: v1
kind: Secret
metadata:
    name: mysql-secret
    namespace: default
stringData:
    MYSQL_USER: ENC[AES256_GCM,data:vhZIVA==,iv:BiJo57ujP8XNneDguI5Ad3/OLw8d6klNT+yKEyHFE10=,tag:zyjo3yPmCtlSOWf8osNYTg==,type:str]
    MYSQL_PASSWORD: ENC[AES256_GCM,data:ACGLoMlQzmCPluOjrJmgm/MAjnGjBG19,iv:Xw0+xMhbHLUQRSPqsN1ftUR+7QhKDgen97jiFyNdnTE=,tag:4/E0/Tsx7bZtpzVr2YaROg==,type:str]
sops:
    kms: []
    gcp_kms: []
    azure_kv: []
    hc_vault: []
    age:
        - recipient: age1ckvt4wkyed9fmz8c32f9mvjwmzlgpfrr5ynmvum4mkanhg0weepq2h48f6
          enc: |
            -----BEGIN AGE ENCRYPTED FILE-----
            YWdlLWVuY3J5cHRpb24ub3JnL3YxCi0+IFgyNTUxOSA5TTdadlhNaE1hVmJSNDRi
            SEVFaElBdENRQnVVeWg3T1NDZWpheFhmcjEwCjFmMFZhVGs0b2NnMGNMYTlOVThh
            dkc3ZzZ5TFZScU5GSlNwcEtMZ3ZzUmcKLS0tIDhVczNtUlFLcnk0VW1UQnJTNkZO
            S2tjeGpBSDFFQmVxcVdMQ05rUUxUVmsKNsDkuC4wSZBCH0m05sbENsR3wCZszZwT
            l0MMLXbPy33RgVnXtxB3JlljfwGf6OMhBk121zZJTCLlTZD2fxhTIg==
            -----END AGE ENCRYPTED FILE-----
    lastmodified: "2024-03-14T11:01:20Z"
    mac: ENC[AES256_GCM,data:sjwL8sRhaSXATNsIaz8tGci/TrQ0WVWr6CCHyRoWI6XwLNVA9GCJQcyvGD2Vlxq0f1dWVTKCoIcpvFQ9eb26zs92MTnO/15FdGRBE0XrRtW1fXwO8f3UqI5CTVAJgtVPNWO2cBGlmR75gMXeat54g721QK1erEei7m1jYoxL2+o=,iv:d9yqilUpVY/z9t9pJyMJMzJtnLPdPx8A7AyK0XngVRw=,tag:Ty9avzsXYNpMnyDl6llCcA==,type:str]
    pgp: []
    encrypted_regex: ^(data|stringData)$
    version: 3.8.1`


    5. Decrypt the file
    sops --decrypt --age $(cat $SOPS_AGE_KEY_FILE | grep -oP "public key: \K(.*)") --encrypted-regex '^(data|stringData)$' --in-place ./secret.yaml

    The file now looks like this:
    `apiVersion: v1
kind: Secret
metadata:
    name: mysql-secret
    namespace: default
stringData:
    MYSQL_USER: root
    MYSQL_PASSWORD: super-Secret-Password!!!`


6. Adding sops plugin to VSCode.

@signageos/vscode-sops-beta support sops and age together.

Click on the encrypted file. It should create an unencrypted file with the name .decrypted-secret.yaml.
We can edit the .decrypted-secret.yaml, the plugin will instantly encrypt the .decrypted-secret.yaml.

*Important*
This plugin will automatically decrypt the files by clicking them. It's super easy to commit and push them. Add 
` *.decrypted~*` in gitignore.

7. encrypt a file.
sops --encrypt --age $(cat $SOPS_AGE_KEY_FILE | ggrep -oP "public key: \K(.*)") -i secret.env
The file looks like this:
`MYSQL_USER=ENC[AES256_GCM,data:Kav8eA==,iv:KzaQUY0QKf8xT0Fgi0pHawYWutF9V+apSiF3PhSMxQo=,tag:tCGofExN18dj95L0Frs5Jg==,type:str]
MYSQL_PASSWORD=ENC[AES256_GCM,data:f4GO8Cr9nQFHaoxTJ1TMKU54gdk6ELOb6DSMRA==,iv:04YVzK5t65P0k6NyoIJVGycjxbt7S4E/q0bYNG8YkB0=,tag:nlb9FqJJLy++dtba1EZyzg==,type:str]
sops_age__list_0__map_enc=-----BEGIN AGE ENCRYPTED FILE-----\nYWdlLWVuY3J5cHRpb24ub3JnL3YxCi0+IFgyNTUxOSA5OUs3Tk50ejhCRFdteXRH\nVDhPaFJ0bitqVWxyNXhzS1R1emtSVzlheXlVClprS0VhT1poK0pKUUhWbDM0ZmF6\ncUVDV2VtcUNTLzVkWmFpWEJRUVVjODAKLS0tIElrZXRnRkhSbldwVCtOUDRieEU5\nLzNZczRtZ0FrZEIzdEdrOHBvM2I0M2sKNvUgvaaDUD5/xztBqbxaQkjXLKeHrQEF\noGGPWwBKeYfO2Ds6F9IGRpJwTP0pdrSElj5jmu8wiqO91bCfVALFbg==\n-----END AGE ENCRYPTED FILE-----\n
sops_age__list_0__map_recipient=age1ckvt4wkyed9fmz8c32f9mvjwmzlgpfrr5ynmvum4mkanhg0weepq2h48f6
sops_lastmodified=2024-03-14T11:36:35Z
sops_mac=ENC[AES256_GCM,data:EuMR4ylHqko6joX7nqSmloqdPgTh+BwtbpcLO5PlPo/BD6rNdoJ6EOF8FOqUaJ2c9/WsujpMGaKOObmtomw/86xvbUFj/kjap1ruEiyCVB5d3laVfPaA+fqZNZhyRQEMJ6xhSriMdQ+X2q8LV5JNKNNTiVs1BJYwIdb0gfNB1YM=,iv:0Txcs7oyy/xioiqJYbwPURTuDpNux8Dafj8/B8cHhIc=,tag:tpA5imcckcR90sXuXxB3hw==,type:str]
sops_unencrypted_suffix=_unencrypted
sops_version=3.8.1`

Decrypt the file: 
sops --decrypt --age $(cat $SOPS_AGE_KEY_FILE | ggrep -oP "public key: \K(.*)") -i secret.env
`MYSQL_USER=root
MYSQL_PASSWORD=super-Secret-Password!!!test`


Supply a secret (age identity) to your cluster (Needed to decrypt the SopsSecret CRDs)
cat <<EOF | kubectl apply -f -
apiVersion: v1
data:
  key: YOUR_BASE64_ENCODED_IDENTITY_PRIVATE_KEY
kind: Secret
metadata:
  name: sops-age-key-file
  namespace: YOUR_NAMESPACE
type: Opaque
EOF