repo name: Kafka-AKS
The kafka user is being created using Terraform.
For a new user we will need to restart the Kafka deployment. 
Restarting the pods, is not enough.
The customer should not understand something. 

New user: add the user to the keyvault pipeline.
Repo azure/aks-poc/keyvault.

The user has to be added in the end of the array. It matters only for the mongoDB users, not for the kafka user.


In the kafka-aks repo we are creating a new tag. Create release, and run the pipeline in the desired environement. 
The pipeline has 3 steps.
1. the operator.This step will configure how many topics we like. how many kafka/zookeeper deployments. We can skip this, but we have agreed to always run this step.
2. Production deployment for kafka. It will also print which users will be added. So the new user should be printed during the pipeline execution.
3. The last step of the pipeline is to test the users. 

Kafka needs to be restarted in order to get the changes.
1st the zookeeper then the kafka pods.
The restart of the pods should be completed by the helm and not manually. 