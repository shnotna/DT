# Login
az login

# Account group commands
# az account : Manage Azure subscription information.

# Show active subscription
az account show --output table

# Set active subscription
#az account set --subscription "dtit_cnd0002" # For SandBox resources
az account set --subscription "dtit_cnp0020" # For PreProd and Prod resources

# Obtain AKS access using az cli
az login
az account set --subscription "dtit_cnp0020"

# For EKS:
az aks get-credentials --name <aks-name> --resource-group <rg>

# For SandBox:
az aks get-credentials --name="aks-sandbox-01" --resource-group "rg-sandbox-backend"

# For PreProd:
az aks get-credentials --name="aks-preprod-01" --resource-group "rg-preprod-backend"

# For Prod:
az aks get-credentials --name="aks-production-01" --resource-group "rg-production-backend"

# Download a certificate. Public and private
az keyvault secret download --vault-name "va-sandbox-01-mh" --name "wildcard-apps-aks-sandbox-01-magentazuhause-app" --file test.pem

In order to see the certificates in Azure , we must add our public ip in:
Key vault -> va-sandbox-01-mh -> Settings -> Networking -> Firewall + Add your client ip address. 
https://portal.azure.com/#@telekomit.onmicrosoft.com/resource/subscriptions/c79ffcfb-fde3-4796-903f-416ca624df90/resourceGroups/rg-sandbox-01-security/providers/Microsoft.KeyVault/vaults/va-sandbox-01-mh/networking

After doing that we can see the certificates in Key vault -> va-sandbox-01-mh -> Settings -> Object -> Certificates