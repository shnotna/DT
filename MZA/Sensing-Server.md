# Sensing Server

The sensing server is detecting motion in the house by monitoring the wifi client devices signals
https://www.originwirelessai.com/

The web gui pipeline is fetching secrets from gitlab variables. We should move them to Azure vault.
Terraform is creating the secrets. So we have to move this to terraform
https://gitlab.devops.telekom.de/mhe/cloud/infra/azure/aks-poc/keyvault

We can remove the sensing server from sandbox.