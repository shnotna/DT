# CMP Project

## Phoenix CMP backend upgrade.

> The Frontend is included. We don't have to do something else besides the backend upgrade.

### Pre-Pord


Phoenix CMP is responsible for upgrading images or factory reset. 
https://gitlab.devops.telekom.de/mhe/cloud/mza/gateway/phoenix-cmp/-/jobs/148657630
This project is using kustomize instead of helm chart. 

Το μόνο που έχουμε να αλλάξουμε το version του image και το κάνουμε push. και πάμε στο pipeline για να τρέξουμε στο env που θέλουμε.
Φτιάχνουμε ένα branch και ζητάμε να μας το κάνουν approve.

Slack Request: https://consumeriot.slack.com/archives/C045YTXQAQ3/p1708512039285899?thread_ts=1708426991.961659&cid=C045YTXQAQ3

Gitlab Repo file: https://gitlab.devops.telekom.de/mhe/cloud/mza/gateway/phoenix-cmp/-/blob/master/control-center/resources/kustomization.yaml?ref_type=heads#L19

GitLab MR: https://gitlab.devops.telekom.de/mhe/cloud/mza/gateway/phoenix-cmp/-/merge_requests/9
Someone has to approve and merge the MR. After that we have to run the pipeline: https://gitlab.devops.telekom.de/mhe/cloud/mza/gateway/phoenix-cmp/-/pipelines/26850734




First we run the preprod-01-build then the preprod-01-sign and last the preprod-01-deploy.
The next step is to verify. The upgrade can be verified by
1. login in to the CMP UI and check for the version
2. check in the K8S env.

**If the deploy build fails, we delete the "cmp-control-center" deployment in phoenix-cmp and re run the deploy pipeline step**

### Deploy to production


Tags > New Tag 
In the Tag name we are inserting the image version. e.g. 1.7.4, in the comments we are sending the message from slack, only the versions. 
Create Tag
A new pipeline will be created. 
We are clicking the pipeline and we are running the production steps